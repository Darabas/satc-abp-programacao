from PyQt5.QtWidgets import QLineEdit, QWidget, QTableWidget, QVBoxLayout, QHBoxLayout, QPushButton
from sqlalchemy import or_

from lab import database
from lab.widgets import QCustomTableItem

class ClientTable(QWidget):
    def __init__(self,  *args):
        QWidget.__init__(self, *args)
        self.session = database.getSession()
        # Layout vertical
        mainLayout = QVBoxLayout(self)

        # Layout horizontal para input e botao
        filterLayout = QHBoxLayout()
        self.filterInput = QLineEdit()
        self.filterInput.setPlaceholderText('Filtrar usuário por código, nome ou CPF')
        self.filterInput.setClearButtonEnabled(True)
        self.filterInput.returnPressed.connect(self.fillTable)
        filterLayout.addWidget(self.filterInput)

        searchButton = QPushButton('Pesquisar')
        searchButton.clicked.connect(self.fillTable)
        filterLayout.addWidget(searchButton)
        filterLayout.addStretch()

        self.table = self.createTable()

        #adiciona o layout do campo de busca e a tabela como elemento principal
        mainLayout.addLayout(filterLayout)
        mainLayout.addWidget(self.table)
        self.fillTable() # preenche os dados da tabela

    def createTable(self):
        table = QTableWidget()
        table.setColumnCount(5)
        table.setHorizontalHeaderLabels([ 'Id', 'Nome', 'Nascimento', 'CPF', 'Gênero'])
        return table

 
    def fillTable(self): 
        self.table.setRowCount(0) # limpa a tabela
        query = self.session.query(database.Client)
        
        q = self.filterInput.text()
        if (q):
            # adiciona todas as clausulas em uma lista e depois usa o operador OR para query
            clauses = [
                database.Client.fullName.ilike('%{}%'.format(q)),
                database.Client.cpf.like('%{}%'.format(q))
            ]

            if (q.isnumeric()):
                clauses.append(database.Client.id == int(q))

            query = query.filter(or_(*clauses))

        # print(query)
        for row, client in enumerate(query): # percorre os registros já filtrados do banco e insere na tabela
            self.table.insertRow(row)
            self.table.setRowHeight(row, 50)
            self.table.setItem(row, 0, QCustomTableItem.QCustomTableItem(str(client.id)))
            self.table.setItem(row, 1, QCustomTableItem.QCustomTableItem(str(client.fullName)))
            self.table.setItem(row, 2, QCustomTableItem.QCustomTableItem(str(client.birthDate)))
            self.table.setItem(row, 3, QCustomTableItem.QCustomTableItem(str(client.cpf)))
            self.table.setItem(row, 4, QCustomTableItem.QCustomTableItem(str(client.gender)))

        self.table.resizeRowsToContents()

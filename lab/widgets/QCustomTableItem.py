from PyQt5.QtWidgets import QTableWidgetItem
from PyQt5.QtCore import Qt

class QCustomTableItem(QTableWidgetItem):
    def __init__(self, text):
        QTableWidgetItem.__init__(self)
        self.setTextAlignment(Qt.AlignCenter)
        self.setText(text)

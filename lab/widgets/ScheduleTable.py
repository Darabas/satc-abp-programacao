from PyQt5.QtWidgets import QWidget, QTableWidget, QVBoxLayout, QHBoxLayout, QLineEdit, QPushButton, QDialog

from lab import database, reports
from lab.widgets import QCustomTableItem, QToaster
from lab.dialogs import ServicesDialog

class ScheduleTable(QWidget):
    def __init__(self,  *args):
        QWidget.__init__(self, *args)

        self.session = database.getSession()

        mainLayout = QVBoxLayout(self)

        # Layout horizontal para input e botao
        filterLayout = QHBoxLayout()
        self.filterInput = QLineEdit()
        self.filterInput.setPlaceholderText('Filtrar pelo CPF do usuário')
        self.filterInput.setClearButtonEnabled(True)
        self.filterInput.returnPressed.connect(self.fillTable)
        filterLayout.addWidget(self.filterInput)

        searchButton = QPushButton('Pesquisar')
        searchButton.clicked.connect(self.fillTable)
        filterLayout.addWidget(searchButton)

        reportButton = QPushButton('Relatório')
        reportButton.clicked.connect(lambda: reports.Reports().schedulesReport(self.filterInput.text()))
        filterLayout.addWidget(reportButton)
        filterLayout.addStretch()

        self.table = self.createTable()
        mainLayout.addLayout(filterLayout)
        mainLayout.addWidget(self.table)
        self.fillTable()

    def createTable(self):
        table = QTableWidget()
        table.setColumnCount(7)
        table.setHorizontalHeaderLabels([ 'Id', 'Data & Hora', 'Cliente', 'Valor', 'Cancelado', '', ''])
        return table

    def fillTable(self): 
        self.table.setRowCount(0) # limpa a tabela

        for row, schedule in enumerate(database.Schedule.list(self.filterInput.text()).all()):
            cancelButton = QPushButton('Cancelar')
            
            # usa o state pq esta dentro de um for
            # https://stackoverflow.com/questions/6784084/how-to-pass-arguments-to-functions-by-the-click-of-button-in-pyqt
            cancelButton.clicked.connect(lambda state, data=schedule: self.cancelSchedule(data))

            serviceButton = QPushButton('Serviços')
            serviceButton.clicked.connect(lambda state, data=schedule: self.openServiceDialog(data))
            
            if schedule.canceled:
                serviceButton.setDisabled(True)
                cancelButton.setDisabled(True)

            self.table.insertRow(row)
            self.table.setRowHeight(row, 50)
            self.table.setItem(row, 0, QCustomTableItem.QCustomTableItem(str(schedule.id)))
            self.table.setItem(row, 1, QCustomTableItem.QCustomTableItem(str(schedule.dateTimeSchedule)))
            self.table.setItem(row, 2, QCustomTableItem.QCustomTableItem(str(schedule.client.fullName)))
            self.table.setItem(row, 3, QCustomTableItem.QCustomTableItem('R$ ' + str(schedule.price)))
            self.table.setItem(row, 4, QCustomTableItem.QCustomTableItem('X' if schedule.canceled else ''))
            self.table.setCellWidget(row, 5, cancelButton)
            self.table.setCellWidget(row, 6, serviceButton)

        self.table.resizeRowsToContents()

    """
    Cancela um agendamento
    """
    def cancelSchedule(self, schedule):
        schedule.canceled = True
        self.session.commit()
        self.fillTable()
        QToaster.QToaster.showMessage(None, "Agendamento {} cancelado".format(schedule.id), timeout=2500)


    """
    Abre o dialogo para adição de serviços no agendamento
    """
    def openServiceDialog(self, schedule):
        dialog = ServicesDialog.ServiceDialog(schedule)
        if dialog.exec() == QDialog.Accepted:
            self.fillTable()

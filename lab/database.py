import enum
from typing import Optional

from sqlalchemy import Column, ForeignKey
from sqlalchemy import Integer, String, Date, DateTime, Enum, Boolean, Float
from sqlalchemy import create_engine
from sqlalchemy.orm import declarative_base, sessionmaker, relationship

from lab import database

ip = 'localhost'
port = '5432'
username = 'postgres'
password = '12345'
db = 'postgres'
url = 'postgresql+psycopg2://{}:{}@{}:{}/{}'

Base = declarative_base()
Session = None

# https://stackoverflow.com/questions/9353822/connecting-postgresql-with-sqlalchemy
# Cria conexão e gera tabelas
def startDatabase():
    try:
        Base.metadata.create_all(getEngine())
    except Exception as e: 
        print(e)

def getSession():
    if not database.Session:
        database.Session = sessionmaker(bind=getEngine())()

    return database.Session

def getEngine():
    return create_engine(
        url.format(username, password, ip, port, db)
    )

class ServicesEnum(enum.Enum):
    GENERAL = { 'description': 'Cliníca Geral', 'price': 150 }
    WHITENING = { 'description': 'Clareamento', 'price': 250 }
    CANAL = { 'description': 'Tratamento de Canal', 'price': 350 }
    IMPLANT = { 'description': 'Implante Dentário', 'price': 500 }

"""
    Tabelas
"""
class Client(Base):
    __tablename__ = 'clients'

    id = Column(Integer, primary_key=True)
    fullName = Column('full_name', String)
    birthDate = Column('birth_date', Date)
    cpf = Column(String(11))
    gender = Column(String(1))
    maritalStatus = Column('marita_status', String(1))

    schedules = relationship('Schedule', back_populates='client')
    
    def __repr__(self):
        return str(self.id) + ": " + self.fullName

class Schedule(Base):
    __tablename__ = 'schedules'

    id = Column(Integer, primary_key=True)
    dateTimeSchedule = Column('date_time_schedule', DateTime)
    canceled = Column(Boolean, default = False)
    price = Column(Float, default = 0)
    
    clientId = Column('client_id', Integer, ForeignKey('clients.id'))
    client = relationship('Client', back_populates='schedules')
    services = relationship('ScheduleServices', back_populates='schedule')

    """
     Cria uma query que busca os agendamentos, filtrando pelo cpf inserido
    """
    def list(cpf: Optional[str] = None):
        query = database.getSession().query(database.Schedule)\
            .order_by(database.Schedule.dateTimeSchedule)

        if cpf:
            # filtra os agendamentos pelo cpf do usuario
            query = query.filter(database.Schedule.client.has(database.Client.cpf.like('%{}%'.format(cpf))))
        
        return query
        
    def __repr__(self):
        return str(self.id)

class ScheduleServices(Base):
    __tablename__ = 'schedules_services'

    id = Column(Integer, primary_key=True)
    service = Column(Enum(ServicesEnum))

    scheduleId = Column('schedule_id', Integer, ForeignKey('schedules.id'))
    schedule = relationship('Schedule', back_populates='services')

    def __repr__(self):
        return str(self.id)

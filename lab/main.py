import sys

from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication, QMainWindow, QAction, QToolBar, QDialog, QVBoxLayout, QWidget
from PyQt5.QtCore import Qt

from lab.dialogs import ClientDialog, ScheduleDialog, ReportServiceDialog, WarnDialog
from lab.widgets import ScheduleTable, ClientTable
from lab import reports

class MainWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.initWindow()
        self.show()

    def initWindow(self):
        self.resize(800, 600)
        self.setWindowTitle('Laboratório')
        self.setWindowIcon(QIcon(r'resources\img\logo_satc.png'))

        """
        Toolbar - adiciona os botões da barra de ações
        """
        toolbar = QToolBar()
        toolbar.setMovable(False)

        clientsAction = QAction(QIcon(r'resources\img\person_add'), 'Clientes', self)
        toolbar.addAction(clientsAction)
        clientsAction.triggered.connect(self.addClient)

        schedulesAction = QAction( QIcon(r'resources\img\calendar_month'), 'Agendamentos', self)
        toolbar.addAction(schedulesAction)
        schedulesAction.triggered.connect(self.addSchedule)

        serviceReport = QAction(QIcon(r'resources\img\description'), 'Relatório de Serviços', self)
        toolbar.addAction(serviceReport)
        serviceReport.triggered.connect(self.servicesReport)

        revenueReport = QAction(QIcon(r'resources\img\money'), 'Relatórios de Receita', self)
        toolbar.addAction(revenueReport)
        revenueReport.triggered.connect(self.revenueReport)

        revenueReport = QAction(QIcon(r'resources\img\logout'), 'Sair', self)
        revenueReport.setShortcut('ctrl+x')
        toolbar.addAction(revenueReport)
        revenueReport.triggered.connect(self.close)

        self.addToolBar(Qt.TopToolBarArea, toolbar)

        """
        Tables adiciona as tabelas de cliente e agendamentos no layout
        """
        centralWidget = QWidget()

        tablesLayout = QVBoxLayout()
        self.clientTable = ClientTable.ClientTable()
        tablesLayout.addWidget(self.clientTable)
        
        self.schedulesTable = ScheduleTable.ScheduleTable()
        tablesLayout.addWidget(self.schedulesTable)

        centralWidget.setLayout(tablesLayout)
        self.setCentralWidget(centralWidget)

    def addClient(self):
        dialog = ClientDialog.ClientDialog() # chama o dialogo de clientes
        try:
            if dialog.exec_() == QDialog.Accepted: # se o usuario cadastrou um novo usuario atualiza a tabela de usuários
                self.clientTable.fillTable()
        except Exception as e:
            WarnDialog.WarnDialog('Erro', 'Erro ao cadastrar usuário').exec()
            print(e)
        
    def addSchedule(self):
        dialog = ScheduleDialog.ScheduleDialog()
        try:
            if dialog.exec() == QDialog.Accepted: # se houve cadastro atualiza a tabela
                self.schedulesTable.fillTable()
        except Exception as e:
            WarnDialog.WarnDialog('Erro', 'Erro ao cadastrar agendamento').exec()
            print(e)
        
    def servicesReport(self):
        dialog = ReportServiceDialog.ReportServiceDialog()
        try:
            dialog.exec()
        except Exception as e:
            print(e)

    def revenueReport(self):
        try:
            reports.Reports().revenueReport()
        except Exception as e:
            print(e)

def run():
    app = QApplication([])
    app.processEvents()

    mainWindow = MainWindow() # é preciso por em uma variavel se n n funciona
    sys.exit(app.exec_())
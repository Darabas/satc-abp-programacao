

from PyQt5.QtWidgets import QMessageBox

class WarnDialog(QMessageBox):
    def __init__(self, title, msg):
        QMessageBox.__init__(self)
        self.setIcon(QMessageBox.Information)
        self.setWindowTitle(title)
        self.setText(msg)
        self.setStandardButtons(QMessageBox.Ok)
from PyQt5 import *
from PyQt5.QtWidgets import QDialog, QPushButton, QHBoxLayout, QFormLayout, QLineEdit, QComboBox, QDateEdit
from PyQt5.QtGui import QRegExpValidator, QIcon
from PyQt5.QtCore import QRegExp, Qt

from lab import database
from lab.dialogs import WarnDialog
from lab.widgets import QToaster

class ClientDialog(QDialog):
    def __init__(self):
        QDialog.__init__(self)
        self.added_pcs = []
        self.initWindow()
        self.initLayout()

    def initWindow(self):
        self.resize(800, 0)
        self.setWindowModality(2)
        self.setWindowTitle('Adicionar Usuário')
        self.setWindowIcon(QIcon(r'resources\img\person.png')) 

    def initLayout(self):
        # cria um formulario e adiciona os inputs
        formLayout = QFormLayout(self)

        self.fullNameInput = QLineEdit()
        self.fullNameInput.setClearButtonEnabled(True)
        formLayout.addRow('Nome Completo', self.fullNameInput)

        self.cpfInput = QLineEdit()
        self.cpfInput.setValidator(QRegExpValidator(QRegExp('\d{11}')))
        self.cpfInput.setMaxLength(11)
        self.cpfInput.setClearButtonEnabled(True)
        formLayout.addRow('CPF', self.cpfInput)

        self.genderCombo = QComboBox(self)
        self.genderCombo.addItem("M")
        self.genderCombo.addItem("F")
        formLayout.addRow('Gênero', self.genderCombo)

        self.maritalStatusCombo = QComboBox()
        self.maritalStatusCombo.addItem("S")
        self.maritalStatusCombo.addItem("C")
        self.maritalStatusCombo.addItem("V")
        formLayout.addRow('Estado Civil', self.maritalStatusCombo)

        self.birthInput = QDateEdit()
        formLayout.addRow('Data de Nascimento', self.birthInput)

        # botões de ação no final do dialogo
        buttonsLayout = QHBoxLayout()
        backButton = QPushButton('Voltar')
        backButton.clicked.connect(lambda: QDialog.close(self))
        self.registerButton = QPushButton('Cadastrar')
        self.registerButton.clicked.connect(self.register)
        buttonsLayout.addWidget(backButton, alignment=Qt.AlignRight)
        buttonsLayout.addWidget(self.registerButton)

        formLayout.addRow(buttonsLayout)

    def register(self): 
        if not self.fullNameInput.text()\
            or not self.cpfInput.text()\
            or not self.genderCombo.currentText()\
            or not self.birthInput.date().toPyDate()\
            or not self.maritalStatusCombo.currentText():
            WarnDialog.WarnDialog('Erro', 'Todos campos devem ser preenchidos').exec()
            return

        # cria um novo cliente e salva no banco de dados
        session = database.getSession()
        client = database.Client(
            fullName = self.fullNameInput.text(),
            cpf = self.cpfInput.text(),
            gender = self.genderCombo.currentText(),
            birthDate = self.birthInput.date().toPyDate(),
            maritalStatus = self.maritalStatusCombo.currentText()
        )
        session.add(client)
        session.flush()
        session.commit()
        QToaster.QToaster.showMessage(None, "Usuário cadastrado", timeout=2500)
        QDialog.accept(self) # finaliza o dialogo


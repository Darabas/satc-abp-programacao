from datetime import datetime

from PyQt5 import *
from PyQt5.QtWidgets import QDialog, QPushButton, QHBoxLayout, QFormLayout, QComboBox, QDateEdit
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import Qt
from sqlalchemy import Date, cast
from sqlalchemy.sql.expression import false

from lab import database
from lab.dialogs import WarnDialog
from lab.widgets import QToaster

class ScheduleDialog(QDialog):
    def __init__(self):
        QDialog.__init__(self)

        clients = database.getSession().query(database.Client).all()
        self.clients = clients

        self.initWindow()
        self.initLayout()

    def initWindow(self):
        self.resize(800, 0)
        self.setWindowModality(2)
        self.setWindowTitle('Realizar Agendamento')
        self.setWindowIcon(QIcon(r'resources\img\calendar_month.png'))

    def initLayout(self):
        formLayout = QFormLayout(self)

        """
        Client Select
        """
        self.clientCombo = QComboBox(self)
        self.clientCombo.setInsertPolicy(QComboBox.NoInsert)
        for client in self.clients: # percorre os clientes e adiciona no combobox
            self.clientCombo.addItem(client.fullName + ' - ' + client.cpf, client)
        
        self.clientCombo.setCurrentIndex(-1) # -1 para começar sem item selecionado
        formLayout.addRow('Cliente', self.clientCombo)

        """
        Date & Hour Select
        """
        self.scheduleDateInput = QDateEdit()
        self.scheduleDateInput.dateChanged.connect(self.updateAvailableHours)
        formLayout.addRow('Data da Consulta', self.scheduleDateInput)

        self.hourCombo = QComboBox(self)
        self.updateAvailableHours()
        formLayout.addRow('Horário da Consulta', self.hourCombo)

        """
        Confirm btns
        """
        buttonsLayout = QHBoxLayout()

        backButton = QPushButton('Voltar')
        backButton.clicked.connect(lambda: QDialog.close(self))

        registerButton = QPushButton('Agendar')
        registerButton.clicked.connect(self.register)

        buttonsLayout.addWidget(backButton, alignment=Qt.AlignRight)
        buttonsLayout.addWidget(registerButton)
        formLayout.addRow(buttonsLayout)

    def updateAvailableHours(self):
        self.hourCombo.clear()
        if not self.scheduleDateInput.date():
            self.hourCombo.setDisabled(True)
            return

        schedules = database.getSession().query(database.Schedule)\
            .filter(cast(database.Schedule.dateTimeSchedule, Date) == self.scheduleDateInput.date().toPyDate())\
            .filter(database.Schedule.canceled == false())\
            .all()
        
        unavailableHours = list(map(lambda schedule: schedule.dateTimeSchedule.time().hour, schedules))
        for hour in (8, 9, 10, 11, 13, 14, 15, 16):
            if hour in unavailableHours:
                continue

            initHour = str(hour).zfill(2)
            endHour = str(hour + 1).zfill(2)
            placeholder = initHour + ':00 - ' + endHour + ':00'
            self.hourCombo.addItem(placeholder, hour)

    # def getHour(self, schedule):
    #     return schedule.dateTimeSchedule.time().hour

    def register(self): 
        if not self.scheduleDateInput.date() or not self.hourCombo.currentData() or not self.clientCombo.currentData():
            WarnDialog.WarnDialog('Erro', 'Todos campos devem ser preenchidos').exec()
            return

        selectedHour = self.hourCombo.currentData()
        scheduleDate = self.scheduleDateInput.date().toPyDate()
        scheduleDateTime = datetime.combine(scheduleDate, datetime(2000, 1, 1, selectedHour).time())
        
        session = database.getSession()
        schedule = database.Schedule(
            dateTimeSchedule = scheduleDateTime,
            clientId = self.clientCombo.currentData().id
        )
        session.add(schedule)
        session.flush()
        session.commit()

        QToaster.QToaster.showMessage(None, "Agendamento cadastrado", timeout=2500)
        QDialog.accept(self)

from PyQt5 import *
from PyQt5.QtWidgets import QDialog, QPushButton, QHBoxLayout, QFormLayout, QComboBox
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import Qt

from lab import database, reports

class ReportServiceDialog(QDialog):
    def __init__(self):
        QDialog.__init__(self)

        clients = database.getSession().query(database.Client).all()
        self.clients = clients

        self.initWindow()
        self.initLayout()

    def initWindow(self):
        self.resize(800, 0)
        self.setWindowModality(2)
        self.setWindowTitle('Relatório de Serviços por Usuário')
        self.setWindowIcon(QIcon(r'resources\img\description.png'))

    def initLayout(self):
        formLayout = QFormLayout(self)

        """
        Client Select
        """
        self.clientCombo = QComboBox(self)
        self.clientCombo.setInsertPolicy(QComboBox.NoInsert)
        for client in self.clients: # percorre os clientes e adiciona no combobox
            self.clientCombo.addItem(client.fullName + ' - ' + client.cpf, client)
        
        self.clientCombo.setCurrentIndex(-1) # -1 para começar sem item selecionado
        formLayout.addRow('Cliente', self.clientCombo)

        """
        Confirm btns
        """
        buttonsLayout = QHBoxLayout()

        backButton = QPushButton('Voltar')
        backButton.clicked.connect(lambda: QDialog.close(self))

        registerButton = QPushButton('Emitir')
        registerButton.clicked.connect(self.report)

        buttonsLayout.addWidget(backButton, alignment=Qt.AlignRight)
        buttonsLayout.addWidget(registerButton)
        formLayout.addRow(buttonsLayout)

    def report(self):
        reports.Reports().servicesReport(self.clientCombo.currentData())
        QDialog.accept(self)

from PyQt5 import *
from PyQt5.QtWidgets import QDialog, QPushButton, QHBoxLayout, QFormLayout, QComboBox
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import Qt

from lab import database
from lab.database import ServicesEnum
from lab.dialogs import WarnDialog
from lab.widgets import QToaster

class ServiceDialog(QDialog):
    def __init__(self, schedule):
        QDialog.__init__(self)
        
        self.schedule = schedule
        self.initWindow()
        self.initLayout()
        self.session = database.getSession()

    def initWindow(self):
        self.resize(800, 0)
        self.setWindowModality(2)
        self.setWindowTitle('Adicionar Serviço')
        self.setWindowIcon(QIcon(r'resources\img\person.png')) 

    def initLayout(self):
        # cria um formulario e adiciona os inputs
        formLayout = QFormLayout(self)

        self.serviceCombo = QComboBox(self)
        self.updateServiceCombo()
  
        # for service in ServicesEnum:
        #     self.serviceCombo.addItem(service.value['description'], service)

        formLayout.addRow('Serviço', self.serviceCombo)

        # botões de ação no final do dialogo
        buttonsLayout = QHBoxLayout()
        backButton = QPushButton('Voltar')
        backButton.clicked.connect(lambda: QDialog.close(self))
        registerButton = QPushButton('Cadastrar')
        registerButton.clicked.connect(self.register)
        buttonsLayout.addWidget(backButton, alignment=Qt.AlignRight)
        buttonsLayout.addWidget(registerButton)

        formLayout.addRow(buttonsLayout)

    def updateServiceCombo(self):
        
        self.serviceCombo.clear()
        scheduleSerivices = database.getSession().query(database.ScheduleServices)\
            .filter(database.ScheduleServices.scheduleId == self.schedule.id)\
            .all()

        unavailableServices = list(map(lambda s: s.service, scheduleSerivices))
        for service in ServicesEnum:
            if service not in unavailableServices:
                self.serviceCombo.addItem(service.value['description'], service)

    def getService(self, scheduleService):
        return scheduleService.service

    def register(self): 
        if not self.serviceCombo.currentText():
            WarnDialog.WarnDialog('Erro', 'Todos campos devem ser preenchidos').exec()
            return

        # self.session.begin()
        schedule = self.schedule
        service = self.serviceCombo.currentData()
        schedule.price = schedule.price + service.value['price']

        service = database.ScheduleServices(
            service = service,
            scheduleId = self.schedule.id
        )
        self.session.add(service)
        self.session.add(schedule)
        self.session.flush()
        self.session.commit()

        QToaster.QToaster.showMessage(None, "Serviço adicionado ao agendamento {}".format(schedule.id), timeout=2500)
        QDialog.accept(self) # finaliza o dialogo

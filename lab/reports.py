import pathlib

from sqlalchemy import func, false
from openpyxl import Workbook

from lab import database
from lab.dialogs import WarnDialog
from lab.widgets import QToaster

class Reports():

    def __init__(self):
        self.reportsDir = '_relatorios'
        # Cria o diretório de relatórios se ele n existe
        path = pathlib.Path(self.reportsDir)
        path.mkdir(parents=True, exist_ok=True)

    """
    Relatório de agendamentos podendo filtrar por cpf
    """
    def schedulesReport(self, cpf = None):
        workbook = Workbook()
        sheet = workbook.active

        # cabecalho da planilha
        header = [ 'Id', 'Data & Hora', 'Cliente', 'Valor', 'Cancelado' ]
        sheet.append(header)

        # adiciona linha das planilhas
        for schedule in database.Schedule.list(cpf):
            row = [
                schedule.id,
                schedule.dateTimeSchedule,
                schedule.client.fullName,
                schedule.price,
                'X' if schedule.canceled else ''
            ]
            sheet.append(row)

        self.saveWorkbook(workbook, 'relatorio-agendamentos')

    """
    Relatório de serviço por usuário
    """
    def servicesReport(self, client):
        if not client:
            WarnDialog.WarnDialog('Erro', 'Deve ser selecionado um usuário!').exec()
            return

        services = database.getSession().query(database.ScheduleServices)\
            .filter(database.ScheduleServices.schedule.has(
                database.Schedule.clientId == client.id
            ))\
            .all()

        workbook = Workbook()
        sheet = workbook.active

        # cabecalho da planilha
        sheet.append(['Serviços referente ao usuário: ' + client.fullName])
        header = [ 'Serviço', 'Data & Hora', 'Valor', 'Cancelado' ]
        sheet.append(header)

        # adiciona linha das planilhas
        for service in services:
            row = [
                service.service.value['description'],
                service.schedule.dateTimeSchedule,
                service.service.value['price'],
                'X' if service.schedule.canceled else ''
            ]
            sheet.append(row)

        self.saveWorkbook(workbook, 'relatorio-servicos')

    def revenueReport(self):
        month = func.date_trunc('month', database.Schedule.dateTimeSchedule)
        tuples = database.getSession().query(
                month,
                func.sum(database.Schedule.price)
            )\
            .filter(database.Schedule.canceled == false())\
            .group_by(month)\
            .all()

        workbook = Workbook()
        sheet = workbook.active

        header = [ 'Mês/Ano', 'Valor' ]
        sheet.append(header)

        # adiciona linha das planilhas
        for tuple in tuples:
            row = [
                '"{}/{}"'.format(tuple[0].month, tuple[0].year),
                tuple[1]
            ]
            sheet.append(row)

        self.saveWorkbook(workbook, 'receita')

    def saveWorkbook(self, workbook, fileName):
        try:
            workbook.save('{}/{}.xlsx'.format(self.reportsDir, fileName))
            QToaster.QToaster.showMessage(None, "Relatório emitido com sucesso", timeout=2500)
            
        except Exception as e:
            print(e)
            WarnDialog.WarnDialog('Erro ao salvar', 'Ocorreu um erro ao salvar o arquivo, verifique se o mesmo está sendo utilizado por outro processo e tente novamente')
    
# ABP Programação - App Laboratório
Projeto final da disciplina de programação o programa deve ser desenvolvido em python e atender as seguintes tarefas

* Cadastrar cliente. 
* Realizar agendamento.
* Cancelar agendamento.
* Cadastro de serviço.
* Relatórios. 

## Dependências
```bash
# Python 3.11
# GUI
pip install pyqt5

# Conexão com o banco
pip install psycopg2

# ORM
pip install SQLAlchemy

#excel
pip install openpyxl
```